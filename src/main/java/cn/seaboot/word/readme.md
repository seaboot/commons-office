
####测试函数

```java
/**
 * @author Mr.css
 * @version 2020-04-08 9:45
 */
public class Test {
  public static void main(String[] args) throws Exception {
    //替换文本
    try (InputStream is = IOUtils.openFileInputStream("C:\\Users\\postm\\Desktop\\ProcessPredictionTemplate.docx");
         OutputStream os = IOUtils.openFileOutputStream("C:\\Users\\postm\\Desktop\\ProcessPredictionTemplate2.docx");
         XWPFDocument docx = new XWPFDocument(is)
    ) {

//      DocPicture picture = new DocPicture();
//      picture.setFile("C:\\Users\\postm\\Desktop\\logo_144.png");
//      picture.setWidth(200);
//      picture.setHeight(200);

      Map<String, Object> map = new HashMap<>();
      map.put("bussiness", "test");
      WordUtils.replaceTag(docx,map, "{{", "}}");
      docx.write(os);
    }
  }
}
```

#### 做水印的时候需要悬于文字下方

```java

public class A{
    public void createPicture(String oid, int id, int width, int height, XWPFParagraph paragraph) {
        final int EMU = 9525;
        width *= EMU;
        height *= EMU;
        String blipId = oid; //getAllPictures().get(id).getPackageRelationship().getId();

//        CTInline inline = paragraph.createRun().getCTR().addNewDrawing().addNewInline();
//        CTAnchor anchor = paragraph.createRun().getCTR().addNewDrawing().addNewAnchor();
        CTDrawing draw = paragraph.createRun().getCTR().addNewDrawing();

        String picXml = "<wp:anchor distT=\"0\" distB=\"0\" distL=\"0\" distR=\"0\" simplePos=\"0\" relativeHeight=\"251658240\" behindDoc=\"0\" locked=\"0\" layoutInCell=\"1\" allowOverlap=\"1\""
                + " xmlns:wp14=\"http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing\" xmlns:wp=\"http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing\">"
                + "<wp:simplePos x=\"0\" y=\"0\"/>"
                + "<wp:positionH relativeFrom=\"character\">"
                + "  <wp:posOffset>0</wp:posOffset>"
                + "</wp:positionH>"
                + "<wp:positionV relativeFrom=\"paragraph\">"
                + "  <wp:posOffset>0</wp:posOffset>"
                + "</wp:positionV>"
//                + "<wp:wrapSquare wrapText=\"bothSides\"/>"
                + "<wp:wrapTopAndBottom/>"
                + "<wp:effectExtent l=\"0\" t=\"0\" r=\"0\" b=\"0\"/>"
                + "<wp:cNvGraphicFramePr>"
                + "  <a:graphicFrameLocks noChangeAspect=\"1\" xmlns:a=\"http://schemas.openxmlformats.org/drawingml/2006/main\"/>"
                + "</wp:cNvGraphicFramePr>"
                + "<a:graphic xmlns:a=\"http://schemas.openxmlformats.org/drawingml/2006/main\">"
                + "   <a:graphicData uri=\"http://schemas.openxmlformats.org/drawingml/2006/picture\">"
                + "      <pic:pic xmlns:pic=\"http://schemas.openxmlformats.org/drawingml/2006/picture\">"
                + "         <pic:nvPicPr>" + "            <pic:cNvPr id=\""
                + id
                + "\" name=\"Generated\"/>"
                + "            <pic:cNvPicPr/>"
                + "         </pic:nvPicPr>"
                + "         <pic:blipFill>"
                + "            <a:blip r:embed=\""
                + blipId
                + "\" xmlns:r=\"http://schemas.openxmlformats.org/officeDocument/2006/relationships\"/>"
                + "            <a:srcRect/>"
                + "            <a:stretch>"
                + "               <a:fillRect/>"
                + "            </a:stretch>"
                + "         </pic:blipFill>"
                + "         <pic:spPr>"
                + "            <a:xfrm>"
                + "               <a:off x=\"0\" y=\"0\"/>"
                + "               <a:ext cx=\""
                + width
                + "\" cy=\""
                + height
                + "\"/>"
                + "            </a:xfrm>"
                + "            <a:prstGeom prst=\"rect\">"
                + "               <a:avLst/>"
                + "            </a:prstGeom>"
                + "         </pic:spPr>"
                + "      </pic:pic>"
                + "   </a:graphicData>"
                + "</a:graphic>"
                + "<wp14:sizeRelH relativeFrom=\"page\">"
                + "  <wp14:pctWidth>0</wp14:pctWidth>"
                + "</wp14:sizeRelH>"
                + "<wp14:sizeRelV relativeFrom=\"page\">"
                + "  <wp14:pctHeight>0</wp14:pctHeight>"
                + "</wp14:sizeRelV>"
                + "</wp:anchor>";

        XmlToken xmlToken = null;
        try {
            xmlToken = XmlToken.Factory.parse(picXml);
        } catch (XmlException xe) {
            xe.printStackTrace();
        }
        draw.set(xmlToken);
        CTAnchor anchor = draw.getAnchorArray(0);
        anchor.addNewGraphic().addNewGraphicData();
//        anchor.set(xmlToken);
//        anchor.setDistT(0);
//        anchor.setDistB(0);
//        anchor.setDistL(0);
//        anchor.setDistR(0);
//        anchor.setAllowOverlap(true);
//        //重点，浮于文字上方
//        anchor.setBehindDoc(false);
//        anchor.setLayoutInCell(true);

//        CTPosH ph = anchor.addNewPositionH();
//        ph.setRelativeFrom(STRelFromH.PAGE);//以整个页面为准的定位
//        ph.setPosOffset(396049);//水平位置，针对RelativeFrom的一个偏移 1厘米=360045 ，1.1*360045
//        CTPosV pv = anchor.addNewPositionV();
//        pv.setRelativeFrom(STRelFromV.PAGE);
//        pv.setPosOffset(468058);//竖直位置，针对RelativeFrom的一个偏移 1.3*360045

        /**
         * 这个是此元素指定的其他程度应添加到每个边缘 （顶部、 底部、 左、 右）
         * 以补偿应用于 DrawingML 对象的任何图形效果的图像
         * 但目前好像用不到
         */
//        CTEffectExtent efextent = anchor.addNewEffectExtent();
//        efextent.setL(19050);
//        efextent.setT(0);
//        efextent.setR(0);
//        efextent.setB(0);

        CTPositiveSize2D extent = anchor.addNewExtent();
        extent.setCx(width);
        extent.setCy(height);

        CTNonVisualDrawingProps docPr = anchor.addNewDocPr();
        docPr.setId(id);
        docPr.setName("Pic_" + id);
        docPr.setDescr("desc");
    }
}
```

```java
public class A{

    public static void main(String[] args) throws IOException {
//        //替换文本
//        try (InputStream is = IOUtils.openInputStream("C:\\Users\\ASUS\\Desktop\\1.docx");
//             OutputStream os = IOUtils.openFileOutputStream("C:\\Users\\ASUS\\Desktop\\2.docx");
//             XWPFDocument docx = new XWPFDocument(is)
//        ) {
//            Map<String,Object> map = new HashMap<>();
//            map.put("sowAngle", "test");
//            replaceText(docx, map, "${", "}");
//            docx.write(os);
//        }

//        //替换图片
//        try (InputStream is = IOUtils.openInputStream("C:\\Users\\ASUS\\Desktop\\1.docx");
//             OutputStream os = IOUtils.openFileOutputStream("C:\\Users\\ASUS\\Desktop\\2.docx");
//             XWPFDocument docx = new XWPFDocument(is)
//        ) {
//            DocPicture picture = new DocPicture();
//            picture.setFile("C:\\Users\\ASUS\\Desktop\\1.png");
//            picture.setWidth(200);
//            picture.setHeight(200);
//
//
//            Iterator<XWPFParagraph> paraIt = docx.getParagraphsIterator();
//            while (paraIt.hasNext()) {
//                XWPFParagraph para = paraIt.next();
//                if(para.searchText("${@imgChartDbz}", new PositionInParagraph()) != null){
//                    replacePicture(para, "${@imgChartDbz}", picture.getInputStream(), picture.getDesc(),
//                            picture.getPictureType(), picture.getWidth(), picture.getHeight());
//                }
//            }
//            docx.write(os);
//        }


        //替换表格
        try (InputStream is = IOUtils.openInputStream("C:\\Users\\ASUS\\Desktop\\1.docx");
             OutputStream os = IOUtils.openFileOutputStream("C:\\Users\\ASUS\\Desktop\\2.docx");
             XWPFDocument docx = new XWPFDocument(is)
        ) {
            List<Map<String, ?>> list = new ArrayList<>();
            Map<String, Object> map = new HashMap<>();
            map.put("sowAngle", "test");
            list.add(map);
            list.add(map);
            list.add(map);
            list.add(map);
            renderTable(docx.getTables().get(0), 1, 1, new String[]{"sowAngle"}, list);
            docx.write(os);
        }
    }
}
```