/*
 * MIT License
 *
 * Copyright (c) 2021 Mr.css
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package cn.seaboot.word;

import cn.seaboot.commons.file.FilenameUtils;
import cn.seaboot.excel.ExcelException;
import org.apache.poi.xwpf.usermodel.Document;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.Serializable;

/**
 * 文档图片，图片最终以流的形式输出，理论上支持所有格式的图片
 * 置入图片前，需保证图片资源无误。
 *
 * @author Mr.css
 * @version 2019/8/6 10:46
 */
public class DocPicture implements Serializable {
    private static final long serialVersionUID = 6821217881317079662L;
    /**
     * 输入流
     */
    private File file;
    /**
     * 图片描述
     */
    private String desc;
    /**
     * 图片类型
     */
    private int pictureType = Document.PICTURE_TYPE_PNG;
    /**
     * 宽度
     */
    private int width = 200;
    /**
     * 高度
     */
    private int height = 200;

    public DocPicture() {
    }

    public DocPicture(@NotNull File file) {
        this.file = file;
        String extension = FilenameUtils.getExtension(file);
        this.pictureType = detectPicTypeByExt(extension);
    }

    public DocPicture(@NotNull File file, int width, int height) {
        this(file);
        this.width = width;
        this.height = height;
    }

    private int detectPicTypeByExt(String ext) {
        if ("png".equalsIgnoreCase(ext)) {
            return Document.PICTURE_TYPE_PNG;
        } else if ("jpg".equalsIgnoreCase(ext) || "jpeg".equalsIgnoreCase(ext)) {
            return Document.PICTURE_TYPE_JPEG;
        } else if ("bmp".equalsIgnoreCase(ext)) {
            return Document.PICTURE_TYPE_BMP;
        } else if ("gif".equalsIgnoreCase(ext)) {
            return Document.PICTURE_TYPE_GIF;
        } else if ("emf".equalsIgnoreCase(ext)) {
            return Document.PICTURE_TYPE_EMF;
        } else if ("wmf".equalsIgnoreCase(ext)) {
            return Document.PICTURE_TYPE_WMF;
        } else if ("dib".equalsIgnoreCase(ext)) {
            return Document.PICTURE_TYPE_DIB;
        } else if ("tiff".equalsIgnoreCase(ext)) {
            return Document.PICTURE_TYPE_TIFF;
        } else if ("eps".equalsIgnoreCase(ext)) {
            return Document.PICTURE_TYPE_EPS;
        } else if ("wpg".equalsIgnoreCase(ext)) {
            return Document.PICTURE_TYPE_WPG;
        } else if ("pict".equalsIgnoreCase(ext)) {
            return Document.PICTURE_TYPE_PICT;
        } else {
            throw new ExcelException("未找到此类型图片！");
        }
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getPictureType() {
        return pictureType;
    }

    public void setPictureType(int pictureType) {
        this.pictureType = pictureType;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    @Override
    public String toString() {
        return "DocPicture{" +
                "file=" + file +
                ", desc='" + desc + '\'' +
                ", pictureType=" + pictureType +
                ", width=" + width +
                ", height=" + height +
                '}';
    }
}
