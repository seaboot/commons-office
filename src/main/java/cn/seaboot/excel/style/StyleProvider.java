package cn.seaboot.excel.style;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Workbook;
import org.jetbrains.annotations.NotNull;

/**
 * 样式提供者
 *
 * 通过 name 获取对应的单元格样式，方便代码进一步封装。
 * 功能上，可以动态创建样式，或者内置一些单元格样式。
 *
 * @author Mr.css
 * @version 2023-01-13 14:56
 * @see cn.seaboot.excel.ExcelBook
 */
public interface StyleProvider {
    /**
     * 标题栏样式
     *
     * @param name     样式名称，能找到对应样式即可
     * @param workbook -
     * @return style
     */
    CellStyle getStyle(@NotNull String name, Workbook workbook);
}
