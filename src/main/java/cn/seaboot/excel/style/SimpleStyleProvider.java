package cn.seaboot.excel.style;

import org.apache.poi.ss.usermodel.*;
import org.jetbrains.annotations.NotNull;

/**
 * 提供一个样例参考，源码并没有直接使用
 *
 * @author Mr.css
 * @version 2023-01-13 14:59
 */
public class SimpleStyleProvider implements StyleProvider {

    /**
     * {@inheritDoc}
     *
     * @param workbook -
     * @return style
     */
    @Override
    public CellStyle getStyle(@NotNull String name, Workbook workbook) {
        switch (name) {
            case "title":
                return titleStyle(workbook);
            case "header":
                return headerStyle(workbook);
            case "data":
                return dataStyle(workbook);
            default:
                return null;
        }
    }

    /**
     * 标题栏样式
     *
     * @param workbook -
     * @return style
     */
    public CellStyle titleStyle(Workbook workbook) {
        // 背景色
        CellStyle style = workbook.createCellStyle();
        style.setAlignment(HorizontalAlignment.CENTER);
        style.setVerticalAlignment(VerticalAlignment.CENTER);
        // 字体
        Font font = workbook.createFont();
        font.setFontName("Consolas");
        font.setFontHeightInPoints((short) 16);
        font.setBold(true);
        style.setFont(font);
        return style;
    }

    /**
     * 表头样式
     *
     * @param workbook -
     * @return style
     */
    public CellStyle headerStyle(Workbook workbook) {
        CellStyle style = this.dataStyle(workbook);
        // 背景色，渲染一整行
        style.setAlignment(HorizontalAlignment.CENTER);
        style.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        // 字体
        Font font = workbook.createFont();
        font.setFontName("Consolas");
        font.setFontHeightInPoints((short) 10);
        font.setBold(true);
        font.setColor(IndexedColors.WHITE.getIndex());
        style.setFont(font);
        return style;
    }

    /**
     * 数据单元格样式
     *
     * @param workbook -
     * @return style
     */
    public CellStyle dataStyle(Workbook workbook) {
        CellStyle style = workbook.createCellStyle();
        // 单元格边框
        style.setVerticalAlignment(VerticalAlignment.CENTER);
        style.setBorderRight(BorderStyle.THIN);
        style.setRightBorderColor(IndexedColors.GREY_50_PERCENT.getIndex());
        style.setBorderLeft(BorderStyle.THIN);
        style.setLeftBorderColor(IndexedColors.GREY_50_PERCENT.getIndex());
        style.setBorderTop(BorderStyle.THIN);
        style.setTopBorderColor(IndexedColors.GREY_50_PERCENT.getIndex());
        style.setBorderBottom(BorderStyle.THIN);
        style.setBottomBorderColor(IndexedColors.GREY_50_PERCENT.getIndex());
        // 字体
        Font font = workbook.createFont();
        font.setFontName("Consolas");
        font.setFontHeightInPoints((short) 10);
        style.setFont(font);
        return style;
    }
}
