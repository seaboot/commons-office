package cn.seaboot.excel.style;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Workbook;
import org.jetbrains.annotations.NotNull;

/**
 * 样式是允许为空的，默认全部置空
 *
 * @author Mr.css
 * @version 2023-01-13 14:59
 */
public class DefaultStyleProvider implements StyleProvider {

    /**
     * {@inheritDoc}
     */
    @Override
    public CellStyle getStyle(@NotNull String name, Workbook workbook) {
        return null;
    }
}
