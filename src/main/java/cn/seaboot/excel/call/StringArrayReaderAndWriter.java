/*
 * MIT License
 *
 * Copyright (c) 2021 Mr.css
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package cn.seaboot.excel.call;

import cn.seaboot.excel.ExcelCell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.jetbrains.annotations.NotNull;

/**
 * 字符串数组的读取
 *
 * @author Mr.css
 * @version 2021-01-20 16:37
 */
public class StringArrayReaderAndWriter implements RowReaderAndWriter<String[]> {

    /**
     * 指定数组长度
     * <p>
     * 默认情况下，Excel 中有多少列，就会读取到多长的数组，这种不稳定因素，会对编程造成困难。
     * 如果指定了数组长度，数据读取的时候，会将结果集封装成定长数组，不足部分用 null 补齐。
     */
    private Integer length;

    public StringArrayReaderAndWriter(Integer length) {
        this.length = length;
    }

    public StringArrayReaderAndWriter() {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String[] readRow(@NotNull Row row) {
        int len = length == null ? row.getLastCellNum() : length;
        String[] arr = new String[len];
        for (int j = 0; j < len; j++) {
            arr[j] = ExcelCell.readString(row.getCell(j));
        }
        return arr;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void writeRow(Row row, String[] array, CellStyle style) {
        for (int i = 0; i < array.length; i++) {
            ExcelCell.addCell(row, i, array[i], style);
        }
    }
}
