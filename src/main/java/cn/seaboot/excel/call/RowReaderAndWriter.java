/*
 * MIT License
 *
 * Copyright (c) 2021 Mr.css
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package cn.seaboot.excel.call;

/**
 * 一个空的接口，同时继承写回调和读回调
 * <p>
 * 读取回调接口{@link RowReadCallback}中，程序每读取一行数据，都会执行一次接口回调；
 * 写回调接口{@link RowWriteCallback}与之对应，每写一行数据，执行一次接口回调。
 * 这两个接口的设计本意是：希望开发人员能够自由的控制数据类型转换。但是，很明显，开发人员能在接口中写任何代码。
 * <p>
 * 注意：
 * 在执行回调的时候，IO流始终处于占用状态，尽量不要在回调接口做耗时的操作，避免出现意料之外的错误。
 * 1、使用{@link RowReadCallback}接口，可以从文件件读一行数据，然后向数据库写一行数据。
 * 此时，文件读取的时间，也会算到事务总执行时间中。
 * 同样，文件IO流，也会直到全部数据存储结束，才能被释放。
 * 合理的做法是，全部读取结束，释放文件资源，再开启数据库事务，逐个存储数据。
 * 2、使用{@link RowWriteCallback}接口，也尽量不要在接口回调中，去查询数据字典。
 *
 * @author Mr.css
 * @version 2021-01-29 11:00
 */
public interface RowReaderAndWriter<T> extends RowReadCallback<T>, RowWriteCallback<T> {
}
