/*
 * MIT License
 *
 * Copyright (c) 2021 Mr.css
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package cn.seaboot.excel.call;

import cn.seaboot.commons.core.Converter;
import cn.seaboot.commons.core.StringUtils;
import cn.seaboot.excel.ExcelCell;
import cn.seaboot.excel.header.SchemaMapping;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.jetbrains.annotations.NotNull;

/**
 * 处理数据库映射，将数据处理成方便JDBC处理的格式，可以轻松实现数据导入功能。
 *
 * @author Mr.css
 * @version 2021-01-20 16:37
 */
public class SchemaMappingReaderAndWriter implements RowReaderAndWriter<Object[]> {

    private final SchemaMapping[] mappings;

    public SchemaMappingReaderAndWriter(SchemaMapping[] mappings) {
        this.mappings = mappings;
    }

    /**
     * 生成 insert 语句
     *
     * @param tableName 表名
     */
    public String makeInsertSql(String tableName) {
        StringBuilder sb = new StringBuilder(120);
        String[] columns = new String[mappings.length];
        String[] values = new String[mappings.length];
        for (int i = 0; i < columns.length; i++) {
            columns[i] = mappings[i].getColumnName();
            values[i] = "?";
        }
        sb.append("INSERT INTO ").append(tableName)
                .append("(").append(StringUtils.join(columns))
                .append(") VALUES (").append(StringUtils.join(values)).append(")");
        return sb.toString();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object[] readRow(@NotNull Row row) {
        int len = mappings.length;
        Object[] arr = new Object[len];
        for (int i = 0; i < len; i++) {
            Class<?> type = mappings[i].getJavaType();
            Object v = ExcelCell.readValue(row.getCell(i));
            if (type != null) {
                v = Converter.convert(v, type);
            }
            arr[i] = v;
        }
        return arr;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void writeRow(Row row, Object[] array, CellStyle style) {
        for (int i = 0; i < array.length; i++) {
            ExcelCell.addCell(row, i, array[i], style);
        }
    }
}
