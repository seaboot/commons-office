/*
 * MIT License
 *
 * Copyright (c) 2021 Mr.css
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package cn.seaboot.excel.call;

import cn.seaboot.excel.ExcelCell;
import cn.seaboot.excel.ExcelException;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.jetbrains.annotations.Nullable;

/**
 * 行写入回调
 *
 * @author Mr.css
 * @version 2020-07-16 14:14
 */
@FunctionalInterface
public interface RowWriteCallback<T> {
    /**
     * 写一行数据
     * <p>
     * CellStyle 来源： {@link cn.seaboot.excel.ExcelBook#cloneStyle(int, int)}, {@link cn.seaboot.excel.style.StyleProvider}
     *
     * @param row    Excel行对象，只是创建了Row，Cell还未创建，推荐使用：ExcelCell.addCell(row, i, obj[i], styles[i])
     * @param obj    data
     * @param style 表格样式，如果未指定则是null
     * @throws ExcelException 输出一些异常信息
     * @see ExcelCell
     * @see cn.seaboot.excel.style.StyleProvider
     */
    void writeRow(Row row, T obj, @Nullable CellStyle style) throws ExcelException;
}