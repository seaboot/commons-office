/*
 * MIT License
 *
 * Copyright (c) 2021 Mr.css
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package cn.seaboot.excel.call;

import cn.seaboot.commons.reflect.FieldAccess;
import cn.seaboot.commons.reflect.ObjectField;
import cn.seaboot.excel.ExcelCell;
import cn.seaboot.excel.ExcelException;
import cn.seaboot.excel.ExcelProperty;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.jetbrains.annotations.NotNull;

/**
 * 对象的读取，每一次读取，需要重新声明 ObjectReaderAndWriter
 * Read row of sheet between startRow and lastRow, and fill data into a list.
 *
 * @author Mr.css
 * @version 2021-01-20 16:05
 * @deprecated 实用性很低，考虑删除
 */
@Deprecated
public class AnnotationReaderAndWriter<T> implements RowReaderAndWriter<T> {
    /**
     * 目标类型
     */
    private final Class<T> target;
    /**
     * 字段数组
     */
    private final ObjectField[] fields;

    /**
     * constructor
     *
     * @param target     targetType
     */
    public AnnotationReaderAndWriter(@NotNull Class<T> target) {
        this.target = target;
        this.fields = this.getFieldByAnnotation(target);
    }


    /**
     * 通过Excel注解确定取值顺序。
     * get field array by {{@link ExcelProperty}}
     *
     * @param clazz Class
     * @return Field array
     */
    private ObjectField[] getFieldByAnnotation(Class<T> clazz) {
        FieldAccess fieldAccess = FieldAccess.create(clazz);
        ObjectField[] fields = fieldAccess.getFields();
        // 数组长度比表头长，此处不需要修改
        ObjectField[] res = new ObjectField[fields.length];
        for (ObjectField field : fields) {
            ExcelProperty property = field.getAnnotation(ExcelProperty.class);
            int idx = property.value();
            if (idx > 0 && idx < fields.length) {
                res[idx] = field;
            } else {
                throw new ArrayIndexOutOfBoundsException("ExcelProperty value is too large: " + property);
            }
        }
        return res;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T readRow(@NotNull Row row) throws ExcelException {
        try {
            T inst = target.newInstance();
            for (int i = 0; i < fields.length; i++) {
                ObjectField field = fields[i];
                if (field != null) {
                    Object object = ExcelCell.readValue(row.getCell(i));
                    field.setValue(inst, object);
                }
            }
            return inst;
        } catch (InstantiationException | IllegalAccessException e) {
            throw new ExcelException("Can not create instance: " + target, e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void writeRow(Row row, T obj, CellStyle style) {
        for (int i = 0; i < fields.length; i++) {
            ObjectField field = fields[i];
            if (field != null) {
                ExcelCell.addCell(row, i, fields[i].getValue(obj), style);
            }
        }
    }
}