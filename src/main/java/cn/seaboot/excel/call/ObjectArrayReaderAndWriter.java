/*
 * MIT License
 *
 * Copyright (c) 2021 Mr.css
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package cn.seaboot.excel.call;

import cn.seaboot.excel.ExcelCell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.jetbrains.annotations.NotNull;

/**
 * 数组数据的读取
 *
 * @author Mr.css
 * @version 2021-01-20 16:37
 */
public class ObjectArrayReaderAndWriter implements RowReaderAndWriter<Object[]> {

    /**
     * 指定数组长度
     * <p>
     * 默认情况下，Excel 中有多少列，就会读取到多长的数组，数组长度不固定，会对编程造成困难。
     * 如果指定了数组长度，数据读取的时候，会将结果集封装成定长数组，不足部分用 null 补齐。
     */
    private Integer length;

    public ObjectArrayReaderAndWriter(Integer length) {
        this.length = length;
    }

    public ObjectArrayReaderAndWriter() {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object[] readRow(@NotNull Row row) {
        int len = length == null ? row.getLastCellNum() : length;
        Object[] arr = new Object[len];
        for (int j = 0; j < len; j++) {
            arr[j] = ExcelCell.readValue(row.getCell(j));
        }
        return arr;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void writeRow(Row row, Object[] array, CellStyle style) {
        for (int i = 0; i < array.length; i++) {
            ExcelCell.addCell(row, i, array[i], style);
        }
    }
}
