/*
 * MIT License
 *
 * Copyright (c) 2021 Mr.css
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package cn.seaboot.excel.call;

import cn.seaboot.excel.ExcelCell;
import cn.seaboot.excel.ExcelException;
import cn.seaboot.excel.header.Header;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Map数据的读写
 *
 * @author Mr.css
 * @version 2021-01-20 16:05
 */
public class MapReaderAndWriter implements RowReaderAndWriter<Map<String, Object>> {

    /**
     * 字段名，用于确定 Map 的取值顺序
     */
    private final String[] fieldNames;

    /**
     * constructor
     *
     * @param fieldNames 字段名
     */
    public MapReaderAndWriter(String[] fieldNames) {
        this.fieldNames = fieldNames;
    }

    /**
     * constructor
     *
     * @param headers fieldNames 不指定，则使用注解确定顺序
     */
    public MapReaderAndWriter(@NotNull Header[] headers) {
        this.fieldNames = Arrays.stream(headers).map(Header::getLabel).toArray(String[]::new);
    }

    /**
     * {@inheritDoc}
     *
     * @param row -
     * @return
     * @throws ExcelException
     */
    @Override
    public Map<String, Object> readRow(@NotNull Row row) throws ExcelException {
        Map<String, Object> map = new HashMap<>(fieldNames.length);
        for (int i = 0; i < fieldNames.length; i++) {
            map.put(fieldNames[i], ExcelCell.readValue(row.getCell(i)));
        }
        return map;
    }

    /**
     * {@inheritDoc}
     *
     * @param row   Excel行对象，只是创建了Row，Cell还未创建，推荐使用：ExcelCell.addCell(row, i, obj[i], styles[i])
     * @param obj   data
     * @param style 表格样式，如果找不到行，则返回空的数组
     */
    @Override
    public void writeRow(Row row, Map<String, Object> obj, CellStyle style) {
        Object value;
        for (int i = 0; i < fieldNames.length; i++) {
            value = obj.get(fieldNames[i]);
            ExcelCell.addCell(row, i, value, style);
        }
    }
}