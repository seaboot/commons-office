/*
 * MIT License
 *
 * Copyright (c) 2021 Mr.css
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package cn.seaboot.excel.call;

/**
 * 一些通用的读写回调
 *
 * @author Mr.css
 * @version 2022-02-24 11:48
 */
public class ExcelCallback {
    /**
     * default Array reader，it can shared by all instance.
     * Maybe I can create more useful RowReadCallback.
     */
    private static final RowReaderAndWriter<Object[]> objectArrayReaderAndWriter = new ObjectArrayReaderAndWriter();
    /**
     * default Array reader，it can shared by all instance.
     * Maybe I can create more useful RowReadCallback.
     */
    private static final RowReaderAndWriter<String[]> stringArrayReaderAndWriter = new StringArrayReaderAndWriter();

    /**
     * return default ArrayReader
     *
     * @return RowReaderAndWriter
     */
    public static RowReaderAndWriter<Object[]> objectArrayCallback() {
        return objectArrayReaderAndWriter;
    }

    /**
     * return default ArrayReader
     *
     * @return RowReaderAndWriter
     */
    public static RowReaderAndWriter<String[]> stringArrayCallback() {
        return stringArrayReaderAndWriter;
    }

    /**
     * return default ArrayReader
     *
     * @return RowReaderAndWriter
     */
    public static RowReaderAndWriter<Object[]> objectArrayCallback(int len) {
        return new ObjectArrayReaderAndWriter(len);
    }

    /**
     * return default ArrayReader
     *
     * @return RowReaderAndWriter
     */
    public static RowReaderAndWriter<String[]> stringArrayCallback(int len) {
        return new StringArrayReaderAndWriter(len);
    }

    /**
     * return default MapReaderAndWriter
     *
     * @return RowReaderAndWriter
     */
    public static MapReaderAndWriter mapCallback(String[] fieldName) {
        return new MapReaderAndWriter(fieldName);
    }

    /**
     * 根据字段名确定对象的取值顺序。
     * return default ObjectReaderAndWriter
     *
     * @return RowReaderAndWriter
     */
    public static <T> ObjectReaderAndWriter<T> objectCallback(Class<T> type, String[] fieldName) {
        return new ObjectReaderAndWriter<>(type, fieldName);
    }


    /**
     * 使用注解实现数据的读写。
     *
     * @return RowReaderAndWriter
     */
    @SuppressWarnings("deprecation")
    public static <T> AnnotationReaderAndWriter<T> annotationCallback(Class<T> type) {
        return new AnnotationReaderAndWriter<T>(type);
    }
}
