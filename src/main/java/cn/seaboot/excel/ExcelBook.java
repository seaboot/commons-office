/*
 * MIT License
 *
 * Copyright (c) 2021 Mr.css
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package cn.seaboot.excel;

import cn.seaboot.commons.core.Asserts;
import cn.seaboot.commons.lang.Warning;
import cn.seaboot.excel.call.RowReadCallback;
import cn.seaboot.excel.call.RowReaderAndWriter;
import cn.seaboot.excel.header.Header;
import cn.seaboot.excel.style.DefaultStyleProvider;
import cn.seaboot.excel.style.StyleProvider;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.ooxml.POIXMLDocumentPart;
import org.apache.poi.openxml4j.opc.PackagePartName;
import org.apache.poi.openxml4j.opc.PackageRelationship;
import org.apache.poi.openxml4j.opc.TargetMode;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.*;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * 07 版本的 Excel 工具
 * <p>
 * HSSFWorkbook         03版本
 * XSSFWorkbook         07版本
 * StreamingWorkbook    超大文件读取
 * SXSSFWorkbook        超大文件写入
 * <p>
 * WorkBook：工作簿，一整个 Excel；
 * Sheet：工作表，一份 Excel 可以包含多个工作表；
 * Row：行，每一个表格可以包含很多行；
 * Cell：单元格，每一行包含很多单元格。
 * <p>
 * {@link Workbook}的封装类，主要针对Excel导入导出业务，函数调用上会更加优雅
 *
 * @author Mr.css
 * @version 2022-02-24 10:44
 * @since 2022-12-15 删除泛型，避免产生奇怪的代码警告，使用的时候需要注意对象类型的设置
 */
public class ExcelBook implements Iterable<Row>, Closeable {
    private final static Logger logger = LoggerFactory.getLogger(ExcelBook.class);

    /**
     * 样式提供器，一个方便二次封装的接口
     */
    private final static StyleProvider DEFAULT_STYLE_PROVIDER = new DefaultStyleProvider();

    /**
     * workbook 工作簿
     */
    private Workbook workbook;
    /**
     * sheet 工作表
     */
    private Sheet sheet;
    /**
     * read or write start row number 起始行号
     */
    private Integer startRow;
    /**
     * read last row number 结束行号
     */
    private Integer endRow;
    /**
     * 样式，写入数据的时候有效
     */
    private CellStyle style;

    /**
     * 样式提供器
     */
    private StyleProvider styleProvider = DEFAULT_STYLE_PROVIDER;
    /**
     * 数据读取回调
     */
    @SuppressWarnings(Warning.RAW_TYPES)
    private RowReaderAndWriter rowReaderAndWriter;

    /**
     * Create workbook， always create default sheet, I think a workbook without sheet is useless.
     * <p>
     * 创建一个工作簿，并创建一个工作表 (sheet)
     *
     * @return -
     */
    public static ExcelBook create() {
        ExcelBook builder = new ExcelBook();
        builder.workbook = new XSSFWorkbook();
        builder.sheet = builder.workbook.createSheet();
        return builder;
    }

    /**
     * Create workbook， always create default sheet, I think a workbook without sheet is useless.
     * <p>
     * 创建一个工作簿，并创建一个工作表 (sheet)
     *
     * @param sheetName 工作表名，注意这个不是文件地址
     * @return -
     */
    public static ExcelBook create(String sheetName) {
        ExcelBook builder = new ExcelBook();
        builder.workbook = new XSSFWorkbook();
        builder.sheet = builder.workbook.createSheet(sheetName);
        return builder;
    }

    /**
     * Create workbook by a excel stream.
     * <p>
     * 从输入流中，打开一份工作簿。输入流中，一般是一个文件模版，或者是已经有数据的文件
     *
     * @param is is
     * @return -
     */
    public static ExcelBook create(InputStream is) {
        try {
            ExcelBook builder = new ExcelBook();
            builder.workbook = WorkbookFactory.create(is);
            return builder;
        } catch (EncryptedDocumentException | IOException e) {
            throw new ExcelException("read excel failed!check your file!", e);
        }
    }

    /**
     * builder.workbook convert to SXSSFWorkbook, start more slowly, but use less memory.
     * this workbook can be used to deal with large excel when writing.
     * <p>
     * 切换成超大文件写入模式
     *
     * @param rowAccessWindowSize rowAccessWindowSize
     * @return ExcelBuilder
     * @see SXSSFWorkbook
     * @since Excel.2007 (.xlsx)
     */
    public ExcelBook asSXSSFWorkbook(int rowAccessWindowSize) {
        this.workbook = new SXSSFWorkbook((XSSFWorkbook) this.workbook, rowAccessWindowSize);
        return this;
    }

    /**
     * Get the Sheet object at the given index.
     * <p>
     * 设置当前使用的工作表
     *
     * @param idx of the sheet number (0-based physical & logical)
     * @return -
     */
    public ExcelBook sheetAt(int idx) {
        sheet = workbook.getSheetAt(idx);
        return this;
    }

    /**
     * Get sheet with the given name
     * <p>
     * 设置当前使用的工作表
     *
     * @param name of the sheet
     * @return -
     */
    public ExcelBook sheetAt(String name) {
        sheet = workbook.getSheet(name);
        return this;
    }

    /**
     * 返回指定行号的 {@link Row} ，在制作表头的时候，会非常实用。
     *
     * @param idx Excel行号
     * @return Row
     */
    public Row getRow(int idx) {
        return this.sheet.getRow(idx);
    }

    /**
     * 返回当前使用的 {@link Sheet} ，可能程序员想自己操作 Excel。
     *
     * @return Sheet
     */
    public Sheet getSheet() {
        return this.sheet;
    }

    /**
     * set read or write start row number
     *
     * @param rowNum row number
     * @return -
     */
    public ExcelBook setStartRow(int rowNum) {
        startRow = rowNum;
        return this;
    }

    /**
     * 采用左闭右开的方式，如果希望操作的最后一行为3，rowNum的值需要传入4
     * set last row number for reading data
     *
     * @return -
     */
    public ExcelBook setEndRow(int rowNum) {
        this.endRow = rowNum - 1;
        return this;
    }

    /**
     * Only in reading, set endRow as number of the last row which has data.
     * <p>
     *
     * @return -
     */
    public ExcelBook setLastRowAsEndRow() {
        endRow = sheet.getLastRowNum();
        return this;
    }

    /**
     * 获取 Excel 中的总行数
     * <p>
     * 不提供超大文件的行号读取，{@link Sheet} 的本质是 {@link Iterator}，是无法回退遍历的。
     *
     * @return 行号
     */
    public int getLastRowNum() {
        return this.sheet.getLastRowNum();
    }

    /**
     * 设置读写回调
     *
     * @param rowReaderAndWriter 读写回调
     * @return this
     */
    public <T> ExcelBook setRowReaderAndWriter(RowReaderAndWriter<T> rowReaderAndWriter) {
        this.rowReaderAndWriter = rowReaderAndWriter;
        return this;
    }

    /**
     * 单元格样式，这个样式会被用于构建新的单元格
     *
     * @param style 单元格样式
     * @return this
     */
    public ExcelBook setStyle(CellStyle style) {
        this.style = style;
        return this;
    }

    /**
     * 创建一个能被当前作业簿使用的样式
     *
     * @return CellStyle
     */
    public CellStyle createCellStyle() {
        return this.workbook.createCellStyle();
    }

    /**
     * 单元格样式，这个样式会被用于构建新的单元格
     *
     * @param provider 样式提供器
     * @return this
     */
    public ExcelBook setStyle(Function<Workbook, CellStyle> provider) {
        this.style = provider.apply(this.workbook);
        return this;
    }

    /**
     * 单元格样式，这个样式会被用于构建新的单元格
     *
     * @param name 单元格样式名称
     * @return this
     */
    public ExcelBook setStyle(@NotNull String name) {
        this.style = this.styleProvider.getStyle(name, this.workbook);
        return this;
    }

    /**
     * 设置样式提供者
     *
     * @param styleProvider 样式提供者
     */
    public ExcelBook setStyleProvider(StyleProvider styleProvider) {
        this.styleProvider = styleProvider;
        return this;
    }

    /**
     * 从工作页现有的单元格中，复制一个样式
     *
     * Clone style from excel cell, this style will use for creating new cell.
     *
     * @param row 行号
     * @param col 列号
     * @return this
     */
    public ExcelBook cloneStyle(int row, int col) {
        this.style = this.sheet.getRow(row).getCell(col).getCellStyle();
        return this;
    }

    /**
     * 合并单元格
     *
     * @param region {@link Sheet#addMergedRegion(CellRangeAddress)}
     * @return this
     */
    public ExcelBook merge(CellRangeAddress region) {
        sheet.addMergedRegion(region);
        return this;
    }

    /**
     * 合并单元格
     * Creates new cell range. Indexes are zero-based.
     *
     * @param firstRow Index of first row
     * @param lastRow  Index of last row (inclusive), must be equal to or larger than {@code firstRow}
     * @param firstCol Index of first column
     * @param lastCol  Index of last column (inclusive), must be equal to or larger than {@code firstCol}
     * @return this
     */
    public ExcelBook merge(int firstRow, int lastRow, int firstCol, int lastCol) {
        return this.merge(new CellRangeAddress(firstRow, lastRow, firstCol, lastCol));
    }

    /**
     * 从迭代器中读取 Excel 内容，读取内容取决于代码指定的范围。
     * 会因为异常中断数据读取。
     *
     * @param <T> 这里压制了类型转换警告，需要自己注意泛型，避免产生类型转换异常
     * @return list, if endRow below of equals startLow will return a empty list
     * @throws ExcelException come from{{@link RowReadCallback#readRow(Row)}}
     */
    @NotNull
    @SuppressWarnings({Warning.RAW_TYPES, Warning.UNCHECKED})
    public <T> List<T> read() {
        logger.debug("read range: [{}, {}]", this.startRow, this.endRow);
        List res = new ArrayList();
        for (Row row : this) {
            if (row != null) {
                Object value = this.rowReaderAndWriter.readRow(row);
                if (value != null) {
                    res.add(value);
                }
            }
        }
        return res;
    }

    /**
     * 读取单行数据，兼容超大文件的内容读取
     *
     * @param <T> 这里压制了类型转换警告，需要自己注意泛型，避免产生类型转换异常
     * @return idx 行号
     * @throws ExcelException           come from{{@link RowReadCallback#readRow(Row)}}
     * @throws IllegalArgumentException 行号越界，超大型 Excel 不允许使用此 API
     */
    @NotNull
    @SuppressWarnings({Warning.UNCHECKED})
    public <T> T readRow(int idx) {
        Row row = sheet.getRow(idx);
        Asserts.notNull(row, "Row number out of excel size: {}", idx);
        return (T) this.rowReaderAndWriter.readRow(row);
    }

    /**
     * 这种读取方式，只适用于小型 Excel。
     * <p>
     * Excel中没有段落的概念，可以用空行做区分。
     * 从遍历所有单元格，直到遍历结束，或者读取到空行，返回这一段的数据。
     * 读取到空行的时候，指针移到前面一行的位置，方便下一次从该行号开始读取数据。
     *
     * @param <T> 这里压制了类型转换警告，需要自己注意泛型，避免产生类型转换异常
     * @return 如果读到结尾，返回null，否则返回一个list
     * @throws ExcelException come from{{@link RowReadCallback#readRow(Row)}}
     */
    @SuppressWarnings({Warning.RAW_TYPES, Warning.UNCHECKED})
    public <T> List<T> nextParagraph() {
        // 起止行号
        int last = this.endRow == null ? this.sheet.getLastRowNum() : this.endRow;
        int start = this.startRow;

        if (start < last) {
            List res = new ArrayList();
            while (start < last) {
                Row row = sheet.getRow(start);
                if (row != null) {
                    Object value = this.rowReaderAndWriter.readRow(row);
                    if (value != null) {
                        res.add(value);
                    }
                } else {
                    break;
                }
                start++;
            }
            this.startRow = start;
            return res;
        } else {
            return null;
        }
    }

    /**
     * Write data into sheet，
     * <p>
     * 这里压制了类型转换警告，
     * 需要程序员自己处理 {@link RowReaderAndWriter} 与 {@link List} 的泛型，二者需要保持一致。
     *
     * @param list data
     * @return this
     */
    @SuppressWarnings({Warning.RAW_TYPES, Warning.UNCHECKED})
    public ExcelBook write(@NotNull List list) {
        // 写入数据
        int idx = this.startRow;
        for (Object t : list) {
            Row row = sheet.createRow(idx);
            this.rowReaderAndWriter.writeRow(row, t, this.style);
            idx++;
        }
        return this;
    }


    /**
     * 写入表头
     *
     * @param row  行号
     * @param data 表头
     * @return this
     */
    @SuppressWarnings({Warning.UNCHECKED})
    public ExcelBook writeRow(int row, @NotNull Object data) {
        Row r = sheet.createRow(row);
        this.rowReaderAndWriter.writeRow(r, data, this.style);
        return this;
    }

    /**
     * 写入表头
     *
     * @param row  行号
     * @param data 表头
     * @return this
     */
    public ExcelBook writeArray(int row, @NotNull String[] data) {
        Row r = sheet.createRow(row);
        // 写入数据
        for (int i = 0; i < data.length; i++) {
            ExcelCell.addCell(r, i, data[i], this.style);
        }
        return this;
    }

    /**
     * 写入数组
     *
     * @param row  行号
     * @param data 表头
     * @return this
     */
    public ExcelBook writeArray(int row, @NotNull Object[] data) {
        Row r = sheet.createRow(row);
        // 写入数据
        for (int i = 0; i < data.length; i++) {
            ExcelCell.addCell(r, i, data[i], this.style);
        }
        return this;
    }

    /**
     * 写入标题
     *
     * @param title 标题
     * @param row   行号
     * @param span  占用的列
     * @return this
     */
    public ExcelBook writeTitle(int row, int span, @NotNull String title) {
        Cell cell = sheet.createRow(row).createCell(0);
        cell.setCellStyle(this.style);
        ExcelCell.setValue(cell, title);
        if (span > 1) {
            this.merge(row, row, 0, span - 1);
        }
        return this;
    }

    /**
     * 写入表头
     *
     * @param row     行号
     * @param headers 表头
     * @return this
     * @since Excel 2007
     */
    public ExcelBook writeHeader(int row, @NotNull Header[] headers) {
        XSSFSheet st = this.asXSSFSheet();
        XSSFRow r = st.createRow(row);
        XSSFDrawing drawing = st.createDrawingPatriarch();
        // 写入数据
        for (int i = 0; i < headers.length; i++) {
            Header header = headers[i];
            XSSFCell c = r.createCell(i);
            // style
            c.setCellStyle(this.style);
            // value
            c.setCellValue(header.getLabel());

            // comment
            if (header.getComment() != null) {
                XSSFComment comment = drawing.createCellComment(new XSSFClientAnchor(0, 0, 0, 0, 0, 0, 0, 0));
                comment.setString(header.getComment());
                c.setCellComment(comment);
            }
        }
        return this;
    }

    /**
     * 添加水印
     * <p>
     * 要求当前实例必须是{@link XSSFWorkbook}
     *
     * @param bytes  图片 byte 数据
     * @param format E.G.: Workbook.PICTURE_TYPE_PNG
     * @return this
     * @throws IllegalArgumentException 作业空间无法转换为 {@link XSSFWorkbook}
     * @see Workbook
     * @since Excel 2007
     */
    public ExcelBook watermark(byte[] bytes, int format) {
        XSSFWorkbook wb = this.asXSSFWorkbook();
        XSSFSheet st = wb.getSheet(sheet.getSheetName());

        // add picture
        int pictureIdx = wb.addPicture(bytes, format);
        markPicture(wb, st, pictureIdx);
        return this;
    }

    /**
     * 写入水印
     *
     * @param is     输入流
     * @param format E.G.: Workbook.PICTURE_TYPE_PNG
     * @throws IOException add picture failed
     * @see Workbook
     * @since Excel 2007
     */
    public ExcelBook watermark(InputStream is, int format) throws IOException {
        XSSFWorkbook wb = this.asXSSFWorkbook();
        XSSFSheet st = wb.getSheet(sheet.getSheetName());

        // add picture
        int pictureIdx = wb.addPicture(is, format);
        markPicture(wb, st, pictureIdx);
        return this;
    }

    /**
     * 添加水印
     * <p>
     * 要求当前实例必须是{@link XSSFWorkbook}
     *
     * @param image      图片
     * @param formatName 图片类型，E.G.: png
     * @return this
     * @throws IOException              数据格式化异常，无法转换为目标类型的图片
     * @throws IllegalArgumentException 作业空间无法转换为 {@link XSSFWorkbook}
     * @since Excel 2007
     */
    public ExcelBook watermark(BufferedImage image, String formatName) throws IOException {
        // convert to byte array & add picture
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ImageIO.write(image, formatName, bos);

        int format = Workbook.PICTURE_TYPE_PNG;
        if ("jpg".equals(formatName)) {
            format = Workbook.PICTURE_TYPE_JPEG;
        } else if ("jpeg".equals(formatName)) {
            format = Workbook.PICTURE_TYPE_JPEG;
        }
        return this.watermark(bos.toByteArray(), format);
    }

    /**
     * 将图片做成水印
     *
     * @param wb         作业空间
     * @param st         工作表
     * @param pictureIdx 图片ID，文件添加到 Excel 之后产生的一个ID
     * @since Excel 2007
     */
    private void markPicture(XSSFWorkbook wb, XSSFSheet st, int pictureIdx) {
        POIXMLDocumentPart poixmlDocumentPart = wb.getAllPictures().get(pictureIdx);

        PackagePartName ppn = poixmlDocumentPart.getPackagePart().getPartName();
        String relType = XSSFRelation.IMAGES.getRelation();
        // add relation from sheet to the picture data
        PackageRelationship pr = st.getPackagePart().addRelationship(ppn, TargetMode.INTERNAL, relType, null);
        // set background picture to sheet
        st.getCTWorksheet().addNewPicture().setId(pr.getId());
    }

    /**
     * 制作单元格批注
     * <p>
     * 前置要求：单元格必须已经存在
     *
     * @param row     行号
     * @param col     列号
     * @param comment 批注
     * @since Excel 2007
     */
    public ExcelBook comment(int row, int col, String comment) {
        XSSFSheet st = this.asXSSFSheet();

        Row xssfRow = st.getRow(row);
        Asserts.notNull(xssfRow, "cell is undefined: [{}, {}]", row, col);

        Cell xssfCell = xssfRow.getCell(col);
        Asserts.notNull(xssfCell, "cell is undefined: [{}, {}]", row, col);

        XSSFDrawing drawing = st.createDrawingPatriarch();
        XSSFComment cellComment = drawing.createCellComment(new XSSFClientAnchor(0, 0, 0, 0, 0, 0, 0, 0));
        cellComment.setString(comment);

        xssfCell.setCellComment(cellComment);
        return this;
    }

    /**
     * 尝试将 workbook 转换为 {@link XSSFWorkbook}
     * <p>
     * 很多功能，在早期的 Excel 中是没有的，需要通过别的方式实现。
     *
     * @return {@link XSSFWorkbook} 转换失败的时候，返回null
     * @throws ExcelException 转换失败的时候
     * @since Excel 2007
     */
    @NotNull
    private XSSFWorkbook asXSSFWorkbook() {
        if (workbook instanceof XSSFWorkbook) {
            return (XSSFWorkbook) workbook;
        } else if (workbook instanceof SXSSFWorkbook) {
            return ((SXSSFWorkbook) workbook).getXSSFWorkbook();
        } else {
            throw new ExcelException("Workbook is not instance of XSSFWorkbook: " + workbook.getClass());
        }
    }

    /**
     * 尝试将工作表对象转换为 {@link XSSFSheet}
     * <p>
     * 很多功能，在早期的Excel中是没有的，需要通过别的方式实现。
     *
     * @return {@link XSSFSheet}
     * @throws ExcelException 转换失败的时候
     * @since Excel 2007
     */
    @NotNull
    private XSSFSheet asXSSFSheet() {
        if (this.sheet instanceof XSSFSheet) {
            return (XSSFSheet) this.sheet;
        } else if (workbook instanceof SXSSFWorkbook) {
            return ((SXSSFWorkbook) workbook).getXSSFWorkbook().getSheet(this.sheet.getSheetName());
        } else {
            throw new ExcelException("Sheet is not instance of XSSFSheet: " + workbook.getClass());
        }
    }

    /**
     * write excel content into output-stream
     *
     * @param os os
     * @throws IOException -
     */
    public void out(@NotNull OutputStream os) throws IOException {
        this.workbook.write(os);
    }

    /**
     * 从迭代器中读取 Excel 内容，读取内容取决于代码指定的范围。
     *
     * @throws ExcelException come from{{@link RowReadCallback#readRow(Row)}}
     */
    @SuppressWarnings({Warning.UNCHECKED, Warning.RAW_TYPES})
    public void eachRow(Consumer consumer) {
        for (Row row : this) {
            if (row != null) {
                consumer.accept(this.rowReaderAndWriter.readRow(row));
            }
        }
    }

    /**
     * Return a Iterator that can traverse row of sheet between startRow and endRow.
     *
     * @return -
     */
    @NotNull
    @Override
    public Iterator<Row> iterator() {
        // 不设置首末行，则使用默认迭代器
        if (this.startRow == null && this.endRow == null) {
            return this.sheet.iterator();
        }
        // 默认读取到尾行
        if (this.endRow == null) {
            this.setLastRowAsEndRow();
        }
        // 在起止行号之间迭代
        return new Iterator<Row>() {
            private int index = startRow;
            private final int end = endRow;

            @Override
            public boolean hasNext() {
                return index <= end;
            }

            @Override
            public Row next() {
                int row = index;
                index++;
                return sheet.getRow(row);
            }
        };
    }

    /**
     * Closes this stream and releases any system resources associated
     * with it. If the stream is already closed then invoking this
     * method has no effect.
     *
     * @throws IOException f an I/O error occurs
     */
    @Override
    public void close() throws IOException {
        if (this.workbook != null) {
            this.workbook.close();
        }
    }
}
