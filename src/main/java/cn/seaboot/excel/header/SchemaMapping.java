package cn.seaboot.excel.header;

/**
 * 数据库映射，用于数据直接导入数据库。
 *
 * @author Mr.css
 * @version 2023-02-09 15:02
 */
public class SchemaMapping {
    /**
     * 数据库列名
     */
    private String columnName;
    /**
     * 字段中文名
     */
    private Class<?> javaType;

    public SchemaMapping() {
    }

    public SchemaMapping(String columnName, Class<?> javaType) {
        this.columnName = columnName;
        this.javaType = javaType;
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public Class<?> getJavaType() {
        return javaType;
    }

    public void setJavaType(Class<?> javaType) {
        this.javaType = javaType;
    }

    @Override
    public String toString() {
        return "SchemaMapping{" +
                "columnName='" + columnName + '\'' +
                ", javaType='" + javaType + '\'' +
                '}';
    }
}
