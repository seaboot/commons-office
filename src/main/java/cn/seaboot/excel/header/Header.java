package cn.seaboot.excel.header;

/**
 * 表头配置
 *
 * 声明一个 header 对象，代码上可能会更倾向与使用 java 对象，而不是直接使用数组。
 *
 * @author Mr.css
 * @version 2023-02-09 15:02
 */
public class Header {
    /**
     * 字段名
     */
    private String name;
    /**
     * 字段中文名
     */
    private String label;
    /**
     * 批注
     */
    private String comment;

    public Header() {
    }

    public Header(String name, String label, String comment) {
        this.name = name;
        this.label = label;
        this.comment = comment;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public String toString() {
        return "Header{" +
                "name='" + name + '\'' +
                ", label='" + label + '\'' +
                ", comment='" + comment + '\'' +
                '}';
    }
}
