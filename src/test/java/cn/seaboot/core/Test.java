/*
 * MIT License
 *
 * Copyright (c) 2021 Mr.css
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package cn.seaboot.core;

import org.apache.poi.xwpf.usermodel.*;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTStyle;

import java.io.*;
import java.util.Iterator;
import java.util.List;

/**
 * 测试函数
 * <p>
 * <p>
 * IBodyElement -------------------迭代器（段落和表格）
 * XWPFComment -------------------评论（个人理解应该是批注）
 * XWPFSDT
 * XWPFFooter -------------------页脚
 * XWPFFootnotes -------------------脚注
 * XWPFHeader -------------------页眉
 * XWPFHyperlink -------------------超链接
 * XWPFNumbering -------------------编号
 * XWPFParagraph -------------------段落
 * XWPFPictureData -------------------图片
 * XWPFStyles -------------------样式（设置多级标题的时候用）
 * XWPFTable -------------------表格
 *
 * @author Mr.css
 * @version 2021-02-01 11:58
 */
public class Test {


    public static void main(String[] args) throws IOException {
        FileInputStream is = new FileInputStream("C:\\Users\\ASUS\\Desktop\\1.docx");
        try (XWPFDocument document = new XWPFDocument(is)) {

            Iterator<IBodyElement> iterator = document.getBodyElementsIterator();
            while (iterator.hasNext()) {
                IBodyElement element = iterator.next();
                if (element.getElementType() == BodyElementType.PARAGRAPH) {

                    // 处理段落
                    XWPFParagraph para = (XWPFParagraph) element;

                    List<XWPFRun> runs = para.getRuns();
                    for (XWPFRun run : runs) {
                        System.out.println("color: " + run.getColor());
                        System.out.println("font-size: " + run.getFontSize());
                        System.out.println("font-name: " + run.getFontName());
                        System.out.println("font-family: " + run.getFontFamily());
                        System.out.println("lang: " + run.getLang());
                        System.out.println("text: " + run.text());

                        // 如果是引用样式，无法获取字体大小，需要根据段落的 num_id 查找样式

                        // 图片资源
                        List<XWPFPicture> pictures = run.getEmbeddedPictures();
                        System.out.println(pictures.size());

                        // 一般的文档用不上
                        // System.out.println("style: " + run.getStyle());
                        // System.out.println("" + run.getPhonetic());
                        // System.out.println("" + run.getKerning());
                        // System.out.println(run.getEmphasisMark());
                        // System.out.println(run.getCharacterSpacing());
                        // System.out.println(run.getFontName());
                        // System.out.println(run.getVerticalAlignment());

                        // xml
                        // System.out.println(run.getCTR());
                        System.out.println("---------------------------------------------------------------");
                    }

                    // ul 无序列表的角标样式
                    System.out.println("num_text: " + para.getNumLevelText());
                    // ul 嵌套级别
                    System.out.println("num_level: " + para.getNumIlvl());

                    // 引用的样式 ID
                    System.out.println("num_id：" + para.getNumID());

                    // xml
                    // System.out.println(para.getCTP());
                    System.out.println("===============================================================");
                } else if(element.getElementType() == BodyElementType.TABLE){
                    // 处理表格
                    XWPFTable table = (XWPFTable) element;

                    // 跟 excel 的处理方式差别不多
                }
            }

            // 文档的样式列表（全局样式）
            XWPFStyles styles = document.getStyles();

            // 样式总数
            System.out.println(styles.getNumberOfStyles());


            XWPFStyle style = styles.getStyle("3");
            System.out.println(style.getName());

            CTStyle ctStyle = style.getCTStyle();
            // 边框等信息
            System.out.println(ctStyle.getPPr());
            // 字体、大小等信息
            System.out.println(ctStyle.getRPr());
        }
        is.close();
    }
}
