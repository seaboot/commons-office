package cn.seaboot.excel;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * @author Mr.css
 * @version 2023-03-08 9:16
 */
public class Watermark {


    public static void main3(String[] args) throws IOException {
        int width = 300, height = 200;
        Font font = new Font("宋体", Font.PLAIN, 50);

        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_BGR);
        Graphics2D g = image.createGraphics();
        // 设置绘制区域（画布大小）
        g.setClip(0, 0, width, height);
        // 绘制底色
        g.setColor(new Color(0, 0, 0, 80));
        g.fillRect(0, 0, width, height);
        // 设置画笔
        g.setColor(Color.WHITE);
        g.setFont(font);
        g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        // 文字指示工具，用于计算文字宽度
        FontMetrics fm = g.getFontMetrics(font);

        int x, y;

        // 顶格居中
        String str = "test";
        y = fm.getAscent() - fm.getDescent();
        x = (image.getWidth() - fm.stringWidth(str)) / 2;
        g.drawString(str, x, y);


        // 写入第二行数据
        str = "test2";
        y = y + font.getSize();
        x = (image.getWidth() - fm.stringWidth(str)) / 2;
        g.drawString(str, x, y);

        g.dispose();

        File out = new File("C:\\Users\\ASUS\\Desktop\\test.png");
        try (OutputStream os = new FileOutputStream(out)) {
            ImageIO.write(image, "png", os);
        }
    }
}
