/*
 * MIT License
 *
 * Copyright (c) 2021 Mr.css
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package cn.seaboot.excel;

import cn.seaboot.commons.file.IOUtils;
import cn.seaboot.excel.call.ExcelCallback;
import cn.seaboot.excel.header.Header;
import cn.seaboot.excel.style.SimpleStyleProvider;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.eventusermodel.XSSFReader;
import org.apache.poi.xwpf.usermodel.*;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.font.FontRenderContext;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * 测试函数
 *
 * @author Mr.css
 * @version 2021-02-01 11:58
 */
public class Test {


    public static void main(String[] args) throws IOException {
        try (InputStream is = new FileInputStream(new File("D:\\maker\\a.xlsx"))) {
            List<Object[]> list = LargerExcelReader
                    .create(is)
                    .sheetAt(0)
                    .setRowReaderAndWriter(ExcelCallback.objectArrayCallback())
                    .read();
            System.out.println(list.size());
        }
    }


    public static void main3(String[] args) throws IOException {
        Header[] headers = new Header[]{
                new Header("a", "a", "adadssa"),
                new Header("b", "b", "adadssa")
        };
        try (OutputStream os = new FileOutputStream(new File("C:\\Users\\ASUS\\Desktop\\test.xlsx"))) {
            ExcelBook
                    .create()
                    .setRowReaderAndWriter(ExcelCallback.objectArrayCallback())
                    .sheetAt(0)
                    .setStyleProvider(new SimpleStyleProvider())
                    .setStyle("title").writeTitle( 0, 2, "标题")
                    .setStyle("header").writeHeader(1, headers)
                    .out(os);
        }
    }

    public static void main2(String[] args) throws IOException {
        try (OutputStream os = new FileOutputStream(new File("C:\\Users\\ASUS\\Desktop\\test.xlsx"))) {
            ExcelBook
                    .create()
                    .setRowReaderAndWriter(ExcelCallback.objectArrayCallback())
                    .sheetAt(0)
                    .setStyleProvider(new SimpleStyleProvider())
                    .setStyle("title").writeTitle( 0, 2, "标题")
                    .setStyle("header").writeArray(1, new String[]{"a", "b"})
                    .comment(1, 5, "adasda")
                    .comment(1, 1, "adasda")
                    .out(os);
        }
    }


    public static void main1(String[] args) throws IOException {
        //使用模版的写法，使用无参的create()函数，则可以凭空创建Excel
        try (InputStream is = IOUtils.openFileInputStream("C:\\Users\\ASUS\\Desktop\\客户清单导入模板1 - 副本.xlsx");
             ExcelBook excelBook = ExcelBook.create(is)) {

            //设置片号
            excelBook.sheetAt(0);
            //起始行号
            excelBook.setStartRow(0);
            //结束行号，如果不设置，取数时会读到最后一行
            excelBook.setEndRow(100);
            //写入数据时，会复制Excel第0行，第0个单元格样式，也允许自己设置Style对象
            excelBook.cloneStyle(0, 0);
            //读写回调，ExcelCallback中提供了常规对象的读写方式
            excelBook.setRowReaderAndWriter(ExcelCallback.objectArrayCallback());

            //读取0-100行的数据
            List<Object[]> res = excelBook.read();

            for (Object[] arr : res) {
                System.out.println(Arrays.toString(arr));
            }

            //从0行开始，写入数据
//            List<Object[]> data = new ArrayList<>();
//            excelBook.write(data);

            //之前设置的参数是可以复用的，按照前面设置好的参数，读取第二片号的数据
//            List<Object[]> res = excelBook.sheetAt(2).read();
        }
    }
}
