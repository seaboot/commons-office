# commons-office

#### 介绍

基于apache-poi 的 excel 内容读写工具。

1. 简化操作，提供更好的语法糖，可以很优雅地进行数据读写；
2. 提供了丰富的工具类，方便做二次封装，当然，默认的功能也能满足很多应用场景。

#### 安装教程

做成 maven 依赖，或者打成 jar 包引用

#### 数据校验

导入数据的时候，可能希望有个数据校验，目前完全没有这方面的设计，也不准备升级，
因为大部分项目都会包含 hibernate-validator，可以结合一起使用即可。

#### 使用说明

常规的用法，设置好 RowReaderAndWriter 之后，即可读写任意范围的数据。
我们并不希望代码处理很复杂的表头，推荐使用文件模版，程序只负责在模版上填充新的数据。


```java

public class Test {
    public static void main(String[] args) throws IOException {
        //使用模版的写法，使用无参的create()函数，则可以凭空创建Excel
        try (InputStream is = IOUtils.openFileInputStream("C:\\tpl.xlsx");
             ExcelBook<Object[]> excelBook = ExcelBook.create(is)) {

            //设置工作表号
            excelBook.sheetAt(1);
            //起始行号
            excelBook.setStartRow(0);
            //结束行号，如果不设置，取数时会读到最后一行
            excelBook.setEndRow(100);
            //创建新的 Cell 时，会复制 Excel中 [0,0] 单元格样式
            excelBook.cloneStyle(0, 0);
            //读写回调，ExcelCallback 中提供了常规对象的读写方式
            excelBook.setRowReaderAndWriter(ExcelCallback.objectArrayCallback());

            //读取 0-100 行的数据
            List<Object[]> res = excelBook.read();

            //从 0 行开始，写入数据
            List<Object[]> data = new ArrayList<>();
            excelBook.write(data);

            //之前设置的参数是可以复用的，按照前面设置好的参数，读取第二个工作表的数据
            List<Object[]> res = excelBook.sheetAt(2).read();
        }
    }
}
```

#### 超大文件读取

超大文件读取，受限于底层实现，无法指定行号读取，只能选择页号全读

```java
public class Test {
    public static void main(String[] args) throws IOException {
        try (InputStream is = new FileInputStream(new File("D:\\file\\excel\\test.xlsx"))) {
            List<Object[]> list = LargerExcelReader
                    .create(is)
                    .sheetAt(0)
                    .setRowReaderAndWriter(ExcelCallback.objectArrayCallback())
                    .read();
            System.out.println(list.size());
        }
    }
}
```

#### 超大文件写入

与常规读用法一模一样，只是有了切换 SXSSFWorkbook 步骤

```java
public class Test {
    public static void main(String[] args) throws IOException {
        try (OutputStream os = new FileOutputStream(new File("C:\\Users\\ASUS\\Desktop\\test.xlsx"))) {
            ExcelBook
                    .create().asSXSSFWorkbook(1000)
                    .setRowReaderAndWriter(ExcelCallback.objectArrayCallback())
                    .sheetAt(0)
                    .setStartRow(1)
                    .write(content)
                    .out(os);
        }
    }
}
```

#### 标题和表头样式设置

写入数据的时候，需要用到很多样式，这时候可以将样式封装到 StyleProvider，
通过 setStyle() 控制当前写入数据采用的样式。

```java
public class Test {
    public static void main(String[] args) throws IOException {
        try (OutputStream os = new FileOutputStream(new File("C:\\Users\\ASUS\\Desktop\\test.xlsx"))) {
            ExcelBook
                    .create()
                    .setRowReaderAndWriter(ExcelCallback.objectArrayCallback())
                    .sheetAt(0)
                    .setStyleProvider(new SimpleStyleProvider())
                    .setStyle("title").writeTitle( 0, 2, "标题")
                    .setStyle("header").writeArray(1, new String[]{"a", "b"})
                    .out(os);
        }
    }
}
```

#### 添加水印

```java
public class Test {
    public static void main(String[] args) throws IOException {
        BufferedImage image = createWaterMark("XXXXX");
        // 导出到字节流
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ImageIO.write(image, "png", bos);

        try (OutputStream os = new FileOutputStream(new File("C:\\Users\\ASUS\\Desktop\\test.xlsx"))) {
            ExcelBook
                    .create()
                    .sheetAt(0)
                    .watermark(bos.toByteArray())
                    .out(os);
        }
    }
}
```

#### 注解部分

注解部分，使用方式如下。

* 目前设计，存在非常大的问题，就是一个对象实体，只能导出一种格式的 Excel；
* 在业务的不同阶段中，对字段需求会发生变化，需要一种设计，在特定阶段，导出特定格式的 Excel；
* 如果再加入样式，需要考虑的事情就越来越多。

暂时没有特别好的想法，感觉注解的设计非常鸡肋，后续可能直接去除这一功能。

```java
/**
 * @author Mr.css
 * @version 2020-12-08 10:29
 */
public class Person {
    @ExcelProperty(1)
    private String name;

    @ExcelProperty(2)
    private int age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
```

#### 安装教程

最低JDK1.8

#### 参与贡献

1. Mr.css












